package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"gatekeeper/server/config"
	"gatekeeper/server/structs"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"time"

	"io/ioutil"
	"log"
	"net/http"
	"os"
)

// User is a retrieved and authentiacted user.

func AuthLogin(c *gin.Context) {
	var authData structs.Auth
	var user structs.User

	var jwtKey = []byte(os.Getenv("JWT_SECRET"))
	ctx := context.Background()
	conf := config.GoogleAuth()

	if err := c.ShouldBindJSON(&authData); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Handle the exchange code to initiate a transport.
	token, err := conf.Exchange(ctx, authData.Code)

	if err != nil {
		fmt.Printf("%s\n", err.Error())
		return
	}

	// Construct the client.
	googleClient := conf.Client(ctx, token)

	clientResponse, err := googleClient.Get(os.Getenv("GOOGLE_USER_API"))

	if err != nil {
		fmt.Printf("%s\n", err.Error())
		return
	}

	defer clientResponse.Body.Close()
	userData, err := ioutil.ReadAll(clientResponse.Body)

	if err != nil {
		fmt.Printf("%s\n", err.Error())
		return
	}

	log.Println("Resp body: ", string(userData))

	err = json.Unmarshal(userData, &user)

	if err != nil {
		fmt.Printf("%s\n", err.Error())
		return
	}

	//Mamy adres email, który trzeba sprawdzić w bazie czy istnieje
	//userAllowed := user.Email == "cianekjr@gmail.com"

	expirationTime := time.Now().Add(5 * time.Minute)

	claims := &structs.Claims{
		Email: user.Email,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	generatedJwt := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// Create the JWT string
	tokenString, err := generatedJwt.SignedString(jwtKey)

	if err != nil {
		fmt.Printf("%s\n", err.Error())
		return
	}

	c.SetCookie("G_AUTHUSER_H", tokenString, int(expirationTime.Unix()), "/", "backend", false, false)
	c.SetCookie("BACKEND", "dwdwdwdwdw", 24*60*60, "/", ".client.localhost", false, false)
	c.SetCookie("TRA", "dwdwdw22dwdw", 24*60, "", ".client.localhost", true, false)

	c.JSON(200, gin.H{
		"access":  token.AccessToken,
		"refresh": token.RefreshToken,
		"expiry":  token.Expiry,
		"user": user,
	})
}
