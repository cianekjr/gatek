module gatekeeper/server

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fatih/color v1.7.0 // indirect
	github.com/gin-contrib/cors v1.3.0
	github.com/gin-gonic/gin v1.4.0
	github.com/githubnemo/CompileDaemon v1.0.0 // indirect
	github.com/howeyc/fsnotify v0.9.0 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/mattn/go-colorable v0.1.4 // indirect
	golang.org/x/oauth2 v0.0.0-20191122200657-5d9234df094c
)
