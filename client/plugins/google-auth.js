import Vue from 'vue'
import GAuth from 'vue-google-oauth2'

const gauthOption = {
  clientId: '424846620102-htbg0b1javplik53a5gajc38i7dgfnqe.apps.googleusercontent.com',
  prompt: 'consent',
  scope: 'email profile',
  fetch_basic_profile: false
}
Vue.use(GAuth, gauthOption)
