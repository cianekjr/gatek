export default {
  mode: 'universal',
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  css: ['~/assets/main.css'],
  build: {
    hotMiddleware: {
      client: {
        overlay: false
      }
    },
    transpile: ['vue-google-oauth2']
  },
  server: {
    port: 8080,
    host: '0.0.0.0'
  },
  env: {
    API_URL: 'http://localhost:3000/',
    NODE_ENV: 'development'
  },
  loading: { color: '#fff' },
  buildModules: ['@nuxtjs/eslint-module'],
  modules: [
    '@nuxtjs/dotenv',
    '@nuxtjs/axios'
  ],
  plugins: [
    { src: 'plugins/google-auth', mode: 'client' }
  ]
}
